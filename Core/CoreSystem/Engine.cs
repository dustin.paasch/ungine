﻿using Ungine.Core.InputSystem;
using Ungine.Core.SceneSystem;

namespace Ungine.Core.CoreSystem;

public class Engine : Microsoft.Xna.Framework.Game
{
    private static Engine _singletonInstance;
    private GraphicsDeviceManager _graphics;
    public SpriteBatch SpriteBatch;
    public List<Sprite> Sprites = new();
    public BaseGame GameInstance;
    public Input Input { private set; get; }


	public static Engine Get() => _singletonInstance ??= new();

	public Engine()
    {
        this._graphics = new GraphicsDeviceManager(this);
        this.Content.RootDirectory = "Content";
        this.IsMouseVisible = true;
    }

    protected override void Initialize()
    {
        this.Input = new();
        base.Initialize();

		this._graphics.PreferredBackBufferWidth = 1920;
		this._graphics.PreferredBackBufferHeight = 1080;
		this._graphics.ApplyChanges();
    }

    protected override void LoadContent()
    {
        this.SpriteBatch = new SpriteBatch(this.GraphicsDevice);

        Sprite.FallbackTexture = this.Content.Load<Texture2D>("Fallback");
        this.GameInstance.OnInit();
    }

    protected override void Update(GameTime gameTime)
    {
        if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            this.Exit();

        float delta = (float)gameTime.ElapsedGameTime.TotalSeconds;
        
        this.Input.Update();
        this.GameInstance.OnUpdate(delta);

        foreach (Sprite sprite in this.Sprites)
        {
            sprite.Update(delta);
        }

        base.Update(gameTime);
    }

    protected override void Draw(GameTime gameTime)
    {
        this.GraphicsDevice.Clear(Color.CornflowerBlue);

        this.SpriteBatch.Begin();
        foreach (Sprite sprite in this.Sprites)
        {
            sprite.Draw();
        }
        this.SpriteBatch.End();

        base.Draw(gameTime);
    }
}