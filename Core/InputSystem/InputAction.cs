using Ungine.Core.CoreSystem;

namespace Ungine.Core.InputSystem;

public class InputAction
{
    public List<Keys> Keys { private set; get; } = new();
    public string Name { private set; get; }
    public bool IsDown { private set; get; }

    public InputAction(string actionName)
    {
        Engine.Get().Input.AddAction(this);
        this.Name = actionName;
    }

    public void CheckState(KeyboardState keyboardState)
    {
        foreach (Keys key in this.Keys)
        {
            if (keyboardState.IsKeyDown(key))
            {
                this.IsDown = true;
                return;
            }
        }
        this.IsDown = false;
    }
}