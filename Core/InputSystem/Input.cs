
namespace Ungine.Core.InputSystem;

public class Input
{
    private List<InputAction> Actions = new();


    public void AddAction(InputAction action)
    {
        this.Actions.Add(action);
    }


    public void Update()
    {
        KeyboardState state = Keyboard.GetState();

        foreach (InputAction action in this.Actions)
        {
            action.CheckState(state);
        }
    }


    public bool IsAction(string name)
    {
        foreach (InputAction action in this.Actions)
        {
            if (action.Name == name && action.IsDown)
                return true;
        }
        return false;
    }
}