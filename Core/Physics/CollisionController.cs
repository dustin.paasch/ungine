using Ungine.Core.ComponentSystem.Colliders;

namespace Ungine.Core.Physics;

public class CollisionController
{
	private static CollisionController _instance;
	public static CollisionController Get() => _instance ??= new();

	private readonly List<ColliderComponent> _staticColliders = new();
	private readonly List<ColliderComponent> _dynamicColliders = new();

	public void Register(ColliderComponent collider)
	{
		List<ColliderComponent> colliderSet = collider.IsStatic ? this._staticColliders : this._dynamicColliders;

		if (colliderSet.Contains(collider))
			throw new InvalidOperationException("Collider is already registered.");

		colliderSet.Add(collider);
	}

	public void Deregister(ColliderComponent collider)
	{
		List<ColliderComponent> colliderSet = collider.IsStatic ? this._staticColliders : this._dynamicColliders;

		if (!colliderSet.Contains(collider))
			throw new InvalidOperationException("Collider is not registered.");

		colliderSet.Remove(collider);
	}

	public void Update()
	{
		foreach (ColliderComponent thisCollider in this._dynamicColliders)
		{
			foreach (ColliderComponent otherCollider in this.GetAllColliders())
			{
				if (thisCollider == otherCollider)
					continue;

				if (thisCollider.IsColliding(otherCollider))
					thisCollider.InvokeCollisionHook(otherCollider);
			}
		}
	}


	private IEnumerable<ColliderComponent> GetAllColliders()
	{
		foreach (ColliderComponent collider in this._dynamicColliders)
		{
			yield return collider;
		}

		foreach (ColliderComponent collider in this._staticColliders)
		{
			yield return collider;
		}
	}
}
