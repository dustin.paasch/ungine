using Ungine.Core.ComponentSystem;
using Ungine.Core.ComponentSystem.Colliders;
using Ungine.Core.CoreSystem;

namespace Ungine.Core.SceneSystem;

public class Sprite
{
    public static Texture2D FallbackTexture;

    public Texture2D Texture { get; set; }
    public Vector2 Position { get; set; }

    private readonly List<Component> _components = new();

    public Sprite()
    {
        Engine.Get().Sprites.Add(this);

        this.Texture = Sprite.FallbackTexture;
        this.Position = Vector2.Zero;
    }

    public void Update(float time) { }

    public void Draw()
    {
        Engine.Get().SpriteBatch.Draw(this.Texture, this.Position, Color.White);
    }

    public virtual void OnCollision(ColliderComponent collider) { }

    public T AddComponent<T>() where T : Component
    {
        if (this.GetComponent<T>() != null)
            throw new Exception($"Component with type '{typeof(T)}' is already registered for this node.");

        T spawnedComponent = Activator.CreateInstance(typeof(T), this) as T;
        this._components.Add(spawnedComponent);
        return spawnedComponent;
    }

    public void RemoveComponent(Component component)
    {
        if (!this._components.Contains(component))
            throw new NullReferenceException($"Tried to remove component of type '{component.GetType()}' that is not existing.");

        this._components.Remove(component);
    }

    public void RemoveComponent<T>() where T : Component
    {
        Component component = this.GetComponent<T>();

        if (component == null)
            throw new NullReferenceException($"Tried to remove component of type '{typeof(T)}' that is not existing.");

        this.RemoveComponent(component);
    }

    public T GetComponent<T>() where T : Component => (T)this._components.Find((component) => component.GetType() == typeof(T));
    public void PrintComponents() => this._components.ForEach((component) => Console.WriteLine(component.GetType()));
}