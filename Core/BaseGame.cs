using Ungine.Core.CoreSystem;
using Ungine.Core.InputSystem;
using Ungine.Core.Physics;

namespace Ungine.Core;

abstract public class BaseGame
{
    public void Start()
    {
        Engine.Get().GameInstance = this;
        Engine.Get().Run();
		System.Console.WriteLine(Engine.Get().GraphicsDevice.Viewport.Bounds);
    }

    protected Input Input {
        get => Engine.Get().Input;
    }

    public virtual void OnInit() { }

    public virtual void OnUpdate(float delta)
	{
		CollisionController.Get().Update();
	}
}