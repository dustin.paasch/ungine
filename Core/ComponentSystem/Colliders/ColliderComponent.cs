using Ungine.Core.SceneSystem;
using Ungine.Core.Physics;

namespace Ungine.Core.ComponentSystem.Colliders;

public abstract class ColliderComponent : Component
{
	private bool _isStatic = false;
	public bool IsStatic {
		get { return this._isStatic; }
		set {
			CollisionController.Get().Deregister(this);
			this._isStatic = value;
			CollisionController.Get().Register(this);
		}
	}
	protected abstract Rectangle Bounds { get; }

    protected ColliderComponent(Sprite owner) : base(owner)
	{
		CollisionController.Get().Register(this);
	}

	public bool IsColliding(ColliderComponent otherCollider)
	{
		return this.Bounds.Intersects(otherCollider.Bounds);
	}

	public void InvokeCollisionHook(ColliderComponent collider)
	{
		this._owner.OnCollision(collider);
	}
}