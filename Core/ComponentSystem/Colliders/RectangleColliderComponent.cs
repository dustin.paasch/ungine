using Ungine.Core.SceneSystem;

namespace Ungine.Core.ComponentSystem.Colliders;

public class RectangleColliderComponent : ColliderComponent
{
    private Vector2 _extents = Vector2.One * 50;
    public Vector2 Extends {
        get { return this._extents; }
        set { this._extents = value; this._halfExtents = value * 0.5f; }
    }

	protected override Rectangle Bounds => new(
		(int)(this._owner.Position.X - this._halfExtents.X),
		(int)(this._owner.Position.Y - this._halfExtents.Y),
		(int)this.Extends.X,
		(int)this.Extends.Y
	);

	private Vector2 _halfExtents;

    public RectangleColliderComponent(Sprite owner) : base(owner) { }
}