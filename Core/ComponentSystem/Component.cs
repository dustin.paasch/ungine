using Ungine.Core.SceneSystem;

namespace Ungine.Core.ComponentSystem;

public abstract class Component
{
    protected readonly Sprite _owner;

    protected Component(Sprite owner)
    {
        this._owner = owner;
    }
}