using Ungine.Core.ComponentSystem.Colliders;
using Ungine.Core.CoreSystem;
using Ungine.Core.SceneSystem;

namespace Ungine.Game;

public class Ball : Sprite
{
	private readonly RectangleColliderComponent _collider;

	public Ball()
	{
		this.Texture = Engine.Get().Content.Load<Texture2D>("Ball");
		this._collider = this.AddComponent<RectangleColliderComponent>();
        this._collider.Extends = new Vector2(32, 32);
	}

	public override void OnCollision(ColliderComponent collider)
	{
		Console.WriteLine("ball collision");
	}
}
