using Ungine.Core.ComponentSystem.Colliders;
using Ungine.Core.SceneSystem;

namespace Ungine.Game;

public class Paddle : Sprite
{
    private readonly RectangleColliderComponent _collider;

    public Paddle()
    {
        this._collider = this.AddComponent<RectangleColliderComponent>();
        this._collider.Extends = new Vector2(64, 64);
    }

	public override void OnCollision(ColliderComponent collider)
	{
		Console.WriteLine("paddle collision");
	}
}
