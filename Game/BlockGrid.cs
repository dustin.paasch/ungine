namespace Ungine.Game;

public class BlockGrid
{
	private readonly Point _size;
	private readonly Block[,] _blocks;


	public BlockGrid(Point size)
	{
		this._size = size;
		this._blocks = new Block[size.X, size.Y];

		for (int y = 0; y < this._size.Y; y++)
		{
			for (int x = 0; x < this._size.X; x++)
			{
				this._blocks[x, y] = new()
				{
					Position = new Vector2(x * 80, y * 32)
				};
			}
		}
	}
}
