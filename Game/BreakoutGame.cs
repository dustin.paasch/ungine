using Ungine.Core;
using Ungine.Core.ComponentSystem;
using Ungine.Core.InputSystem;
using Ungine.Core.SceneSystem;

namespace Ungine.Game;

public class BreakoutGame : BaseGame
{
    private Paddle _paddle;
    private InputAction _leftAction;
    private InputAction _rightAction;

    public override void OnInit()
    {
        this._leftAction = new("left");
        this._leftAction.Keys.Add(Keys.A);
        this._leftAction.Keys.Add(Keys.Left);

        this._rightAction = new("right");
        this._rightAction.Keys.Add(Keys.D);
        this._rightAction.Keys.Add(Keys.Right);

        this._paddle = new() {
            Position = new Vector2(500, 300)
        };

		new BlockGrid(new Point(24, 12));

		new Ball()
		{
			Position = new Vector2(200, 200)
		};
    }

    public override void OnUpdate(float delta)
    {
		base.OnUpdate(delta);

        const float speed = 150.0f;

        if (this.Input.IsAction("left"))
			this._paddle.Position -= Vector2.UnitX * delta * speed;

        if (this.Input.IsAction("right"))
			this._paddle.Position += Vector2.UnitX * delta * speed;
    }
}
