using Ungine.Core.ComponentSystem.Colliders;
using Ungine.Core.CoreSystem;
using Ungine.Core.SceneSystem;

namespace Ungine.Game;

public class Block : Sprite
{
    private readonly RectangleColliderComponent _collider;

    public Block()
    {
		this.Texture = Engine.Get().Content.Load<Texture2D>("Block");
        this._collider = this.AddComponent<RectangleColliderComponent>();
        this._collider.Extends = new Vector2(64, 64);
		this._collider.IsStatic = true;
    }

	public override void OnCollision(ColliderComponent collider)
	{
		Console.WriteLine("box collision");
	}
}
